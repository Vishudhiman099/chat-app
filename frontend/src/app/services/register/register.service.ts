import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }



  saveData(data:any){
    return this.http.post('http://localhost:3000/data',data)
  }

  getData(){
    return this.http.get('http://localhost:3000/data')
  }
}
