import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService} from '../services/register/register.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private rgService:RegisterService, private router:Router) { }

  ngOnInit(): void {
  }

  submit(data:any){
    this.rgService.saveData(data).subscribe((res)=>{
      this.router.navigate(['login'])
    })
  }
}
